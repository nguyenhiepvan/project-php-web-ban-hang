<?php 
session_start();
if(!isset($_SESSION['prod']['PHOTO'])){
	$_SESSION['prod']['PHOTO'] = 'default.png';
}
if(isset($_GET['mod']) && isset($_GET['act'])){
	$mod = $_GET['mod'];
	$act = $_GET['act'];
	switch ($mod) {
		case 'admin':
		require_once('admin/controllers/admin_controller.php');
		$admin = new admin_controller();
		if($act == 'login') $admin->login();
		if($act == 'login_process') $admin->login_process();

		if(isset($_SESSION['admin'])){
			switch ($act) {
				case 'profile':
				$admin->profile();
				break;

				case 'logout':
				$admin->logout();
				break;

				case 'all_prod':
				$admin->all_prod();
				break;

				case 'all_type':
				$admin->all_type();
				break;

				case 'all_supp':
				$admin->all_supp();
				break;

				case 'get_all_product':
				$admin->get_all_product();
				break;

				case 'get_all_type':
				$admin->get_all_type();
				break;

				case 'get_all_supp':
				$admin->get_all_supp();
				break;

				case 'display_types_of_prod':
				$admin->display_types_of_prod();
				break;

				case 'display_supp':
				$admin->display_supp();
				break;

				case 'insert_prod':
				$admin->insert_prod();
				break;

				case 'insert_supp':
				$admin->insert_supp();
				break;

				case 'insert_type':
				$admin->insert_type();
				break;

				case 'upload_photo_prod':
				$admin->upload_photo_prod();
				break;
				
				case 'detail_prod':
				$admin->detail_prod();
				break;
				
				case 'delete_prod':
				$admin->delete_prod();
				break;

				default:
				break;
			}
		}
		if(!isset($_SESSION['admin']) && $act!='login')
		{
			echo "<script type='text/javascript'>alert('Sorry, you must login first');
			window.location.href = '"."?mod=admin&act=login"."';
		</script>";

		exit;
	}
	break;
	default:
	echo "Không có modul này";
	break;
}
}
else{
	echo "not avalible";
	
}
?>