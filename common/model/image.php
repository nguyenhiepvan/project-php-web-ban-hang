<?php 
/**
* 
*/
require_once('model.php');
class image extends model
{
	var $code_model;
	function __construct()
	{
		parent::__construct();
		$this->setTable('IMAGES');
		$this->setPrimary_key('CODE');
		$this->generate_id('IMG');
	}
	function setCode_model($code_model)
	{
		$this->code_model = $code_model;
	}
	function getCode_model()
	{
		return $this->code_model;
	}
}
?>