<?php 
require_once('model.php');
require_once('supplier.php');
require_once('type_of_product.php');
require_once('image.php');
?>
<?php 
/**
* 
*/
class Product extends model
{
	var $code_supp;
	var $code_type;
	var $discription;

	function __construct()
	{
		parent::__construct();
		$this->setTable('PRODUCTS');
		$this->setPrimary_key('CODE_PRODUCT');
		$this->generate_id('PROD');
	}

	function setCode_supp($code_supp)
	{
		$this->code_supp = $code_supp;
	}
	function setCode_type($code_type)
	{
		$this->code_type = $code_type;
	}
	function setDiscription($discription)
	{
		$this->discription = $discription;
	}

	function getCode_supp()
	{
		return $this->code_supp;
	}
	function getCode_type()
	{
		return $this->code_type;
	}

	function getDiscription()
	{
		return $this->discription;
	}
}

?>