<?php 
/**
* 
*/
require_once('model.php');
class status extends model
{
	var $quantity;
	var $price;
	var $profit;
	var $pending;
	var $sold;
	function __construct()
	{
		parent::__construct();
		$this->setTable('STATUS');
		$this->setPrimary_key('CODE_PRODUCT');
	}

	function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	}
	function setPrice($price)
	{
		$this->price = $price;
	}
	function setProfit($profit)
	{
		$this->profit = $profit;
	}
	function setPending($pending)
	{
		$this->pending = $pending;
	}
	function setSold($sold)
	{
		$this->sold = $sold;
	}

	function getQuantity()
	{
		return $this->quantity;
	}
	function getPrice()
	{
		return $this->price;
	}
	function getProfit()
	{
		return $this->profit;
	}
	function getPending()
	{
		return $this->pending;
	}
	function getSold()
	{
		return $this->sold;
	}
}
?>