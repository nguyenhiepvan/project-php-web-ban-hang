<?php 
/**
* 
*/
require_once('env.php');
class Connection
{
	var $connect;
	function __construct()
	{
		$this->connect = new PDO("mysql:host=".HOST.";dbname=".DBNAME."", USER, PASS);
		$this->connect->exec("set names utf8");
	}
	function getConnection()
	{
		return $this->connect;
	}
}

?>