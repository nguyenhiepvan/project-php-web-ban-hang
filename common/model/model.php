<?php 
/**
* 
*/
//require_once('Connection.php');
require_once('Connection.php');
class model
{
	
	var $name;
	var $code;
	var $img = array();
	var $connect;
	var $table ='';
	var $primary_key ='';
	function __construct()
	{
		
		$connection = new Connection();
		$this->connect = $connection->getConnection();
	}
//hàm get set dữ liệu	
	function setCode($code)
	{
		$this->code = $code;
	}
	function setName($name)
	{
		$this->name = $name;
	}
	function setImg($img)
	{
		$this->img = $img;
	}
	function setTable($table)
	{
		$this->table = $table;
	}
	function setPrimary_key($key)
	{
		$this->primary_key = $key;
	}
	function getCode()
	{
		return $this->code;
	}
	function getName()
	{
		return $this->name;
	}
	function getImg()
	{
		return $this->img;
	}
	function getTable()
	{
		return $this->table;
	}
	function getPrimary_key()
	{
		return $this->primary_key;
	}

	//Hàm hiển thì dữ liệu
	function display()
	{
		echo "<pre>";
		print_r($this);
		echo "</pre>";

	}
	//Hàm tạo id
	function generate_id($key)
	{
		require_once('common/generate_id.php');
		//require_once('../../common/generate_id.php');
		$this->setCode($key.getToken(10));
	}
	//Hàm lấy dữ liệu theo id
	function find($id)
	{ 		
		$query = "SELECT * FROM ".$this->table." 
		WHERE `".$this->primary_key."` ='".$id."';";

		$statement = $this->connect->prepare($query);
		$statement->execute();
		return $statement->fetch();
	}
	//Hàm tìm bản ghi đã tồn tại:
	function is_available($keyword, $key)
	{ 		
		
		$query = "SELECT * FROM ".$this->table." WHERE ".$key." = '".$keyword."'";

		
		$statement = $this->connect->prepare($query);
		$statement->execute();
		return $statement->rowCount();
	}
	//Hàm lấy bản ghi đã tồn tại:
	function get_record($keyword, $key)
	{ 		
		
		$query = "SELECT * FROM ".$this->table." WHERE ".$key." = '".$keyword."'";

		
		$statement = $this->connect->prepare($query);
		$statement->execute();
		return $statement->fetchAll();
	}
	//Hàm lấy tất cả các dữ liệu:
	function All()
	{
		$query = "SELECT * FROM ".$this->table."";
		$statement = $this->connect->prepare($query);
		$statement->execute();
		return $statement->fetchAll();
	}
	//Hàm thêm sản phẩm vào database
	function insert($data)
	{

		$fields = '';
		$values = '';
		foreach ($data as $key => $value) {
			$fields .= $key .',';
			$values .= "'".$value."',";
		}
		$fields = trim($fields,',');
		$values =trim($values,',');
		$query = "INSERT INTO ".$this->table." (".$fields.")
		VALUES (".$values.");";

		$statement = $this->connect->prepare($query);
		return $statement->execute();
	}
	//hảm sửa sản phẩm
	function update()
	{
		$query_temp = '';
		foreach ($data as $key => $value) {
			$query_temp .= $key .'= "'.$value.'",';
		}
		$query = "UPDATE ".$this->table."
		SET ".$query_temp."
		WHERE ".$this->primary_key."= '".$data[$this->primary_key]."'";

		$statement = $this->connect->prepare($query);
		return $statement->execute();
	}
	function delete()
	{
		$query = "DELETE FROM ".$this->table.
		" WHERE ".$this->primary_key."= '".$this->getCode()."'";
		
		$statement = $this->connect->prepare($query);
		return $statement->execute();
	}	
}

?>