<?php 
/**
* 
*/
require_once('model.php');
class type_of_product extends model
{
	var $unit;
	function __construct()
	{
		parent::__construct();
		$this->generate_id('TYPE');
		$this->setTable('TYPES_OF_PRODUCT');
		$this->setPrimary_key('CODE_TYPE');
	}
	function setUnit($unit)
	{
		$this->unit = $unit;
	}
	function getUnit($unit)
	{
		return $this->unit;
	}
}
?>