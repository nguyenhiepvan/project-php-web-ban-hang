<?php 
require_once('model.php');
/**
* 
*/
class supplier extends model
{
	var $country;
	function __construct()
	{
		parent::__construct();
		$this->generate_id('SUPP');
		$this->setTable('SUPPLIERS');
		$this->setPrimary_key('CODE_SUPPLIER');
	}
	function setCountry($country)
	{
		$this->country = $country;
	}
	function GetCountry()
	{
		return $this->country;
	}
}
?>
