$(document).ready(function(){
	$('#login_form').on('submit', function(event){
		event.preventDefault();
		var error_username = '';
		var error_password = '';

		if($('#username').val() == '')
		{
			error_username = ' username is required';
			$('#error_username').text(error_username);
			$('#username').css('border-color', '#cc0000');
		}
		else
		{
			error_username = '';
			$('#error_username').text(error_username);
			$('#username').css('border-color', '');
		}

		if($('#password').val() == '')
		{
			error_password = ' password is required';
			$('#error_password').text(error_password);
			$('#password').css('border-color', '#cc0000');
		}
		else
		{
			error_password = '';
			$('#error_password').text(error_password);
			$('#password').css('border-color', '');
		}

		if(error_username != '' || error_password != '' )
		{
			return false;
		}
		else
		{
			$('#login_form').attr('disabled', 'disabled');
			var form_data = $(this).serialize();
			$.ajax({
				url:"?mod=admin&act=login_process",
				method:"POST",
				data:form_data,
				success:function(data)
				{
					if(data == '1'){
						window.location.href = "?mod=admin&act=profile";
					}
					else{
						toastr.error('Username or Password is Incorrect', 'ERROR ALERT', {timeOut: 1500});
					}
				}
			});

		}
	});
});