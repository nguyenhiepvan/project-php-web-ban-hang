function delete_prod(value)
{
  $.ajax({
    url: '?mod=admin&act=delete_prod&id='+String(value),
    method: 'POST',
    cache: false,
    success: function(data){
      if (data=='1') {
        toastr.success('xóa thành công','SUCCESS ALERT', {timeOut: 1500});
        load_data();
      } else {
        toastr.error('Có gì đó sai sai', 'ERROR ALERT', {timeOut: 1500});
      }
    }
  });
}

function load_data()
{
  $.ajax({
   url: "?mod=admin&act=get_all_product",
   type : 'POST',
   cache: false,
   success: function(res)
   {
    var table_pruduct = $('#productTable').DataTable({
      data: JSON.parse(res),
      responsive: true,
      columns: [
      {data: 'index', name: 'index'},
      {data: 'NAME_IMG',
      "render": function ( data, type, row, meta ){
        return '<img src="common/img/product/'+data+'"  width = "30" height = "30px">';
      }, name: 'NAME_IMG', orderable: false, searchable: false},
      {data: 'NAME_PROD', name: 'NAME_PROD'},
      {data: 'NAME_SUPP', name: 'NAME_SUPP'},
      {data: 'UNIT', name: 'UNIT'},
      {data: 'QUANTITY', name: 'QUANTITY'},
      {data: 'PRICE', name: 'PRICE'},
      {data: 'PROFIT', name: 'PROFIT'},
      {data: 'CODE_PRODUCT',
      "render": function ( data, type, row ) {
        return '<button type="button" onClick="detail_prod(\'' + data + '\')"  class="btn btn-success btn-xs" ><i class="fa fa-plus"></i></button>';
      }, orderable: false, searchable: false
    },
    {data: 'CODE_PRODUCT',
    "render": function ( data, type, row ) {
      return '<button type="button" onClick="update_prod(\'' + data + '\')"  class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>';
    }, orderable: false, searchable: false
  },
  {data: 'CODE_PRODUCT',
  "render": function ( data, type, row ) {
    return '<button type="button"  onClick="delete_prod(\'' + data + '\')"  class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>';
  }, orderable: false, searchable: false
}
],
"bDestroy": true
});
    table_pruduct.draw();
  }
});


  var table_type = $('#typeOfProductTable').DataTable({
    processing: true,
    "ajax": {
      url: "?mod=admin&act=get_all_type",
      dataSrc: "",
      type : 'POST'
    },
    columns: [
    {data: 'CODE_TYPE', name: 'CODE_TYPE'},
    {data: 'NAME_TYPE', name: 'NAME_TYPE'},
    {data: 'UNIT', name: 'UNIT'}
    ]
  });
  table_type.draw();

  var table_supp = $('#suppliertTable').DataTable({
    processing: true,
    "ajax": {
      url: "?mod=admin&act=get_all_supp",
      dataSrc: "",
      type : 'POST'
    },
    columns: [
    {data: 'CODE_SUPPLIER', name: 'CODE_SUPPLIER'},
    {data: 'NAME_SUPP', name: 'NAME_SUPP'},
    {data: 'COUNTRY', name: 'COUNTRY'}
    ]
  });
  table_supp.draw();

  $.ajax({
    url:"?mod=admin&act=display_types_of_prod",
    method:"POST",
    success:function(data)
    {
      $('#data_type').html(data);
    }
  });
  $.ajax({
    url:"?mod=admin&act=display_supp",
    method:"POST",
    success:function(data)
    {
      $('#data_supp').html(data);
    }
  });
}

$(document).ready(function(){  

  load_data();

  $('#add_product_button').click(function(){
    $('#add_prod_form')[0].reset();
    $("#add_product").modal('show');
  });


  $('#add_prod_form').on('submit', function(event){
    event.preventDefault();
    var error_name = '';
    var error_type = '';
    var error_supp = '';

    if($('#NAME_PROD').val() == '')
    {
      error_name = 'Nhập Tên sản phẩm';
      $('#error_name').text(error_name);
      $('#NAME_PROD').css('border-color', '#cc0000');
    }
    else
    {
      error_name = '';
      $('#error_name').text(error_name);
      $('#NAME_PROD').css('border-color', '');
    }

    if($('#TYPE').val() == 'default')
    {
      error_type = 'Chọn loại sản phẩm';
      $('#error_type').text(error_type);
      $('#TYPE').css('border-color', '#cc0000');
    }
    else
    {
      error_type = '';
      $('#error_type').text(error_type);
      $('#TYPE').css('border-color', '');
    }

    if($('#SUPP').val() == 'default')
    {
      error_supp = 'Chọn nhà sản xuất';
      $('#error_supp').text(error_supp);
      $('#SUPP').css('border-color', '#cc0000');
    }
    else
    {
      error_supp = '';
      $('#error_supp').text(error_supp);
      $('#SUPP').css('border-color', '');
    }

    if(error_name != '' || error_type != '' || error_supp != '' )
    {
      return false;
    }
    else
    {
      $('#add_prod_form').attr('disabled', 'disabled');
      var form_data = $(this).serialize();
      $.ajax({
        url:"?mod=admin&act=insert_prod",
        method:"POST",
        data:form_data,
        success:function(data)
        {
          console.log(data);
          if(data ==  '1')
          {
            $('#add_product').modal('hide');
            load_data();
            $('#form_prod_action').attr('disabled', false);
            toastr.success('Thêm thành công nha', 'SUCCESS ALERT', {timeOut: 1500});
          }
          else{
            toastr.error('Có gì đó sai sai 361', 'ERROR ALERT', {timeOut: 1500});
          }
        }
      });

    }
  });
  $('#add_type').click(function(){
    $('#action_type').val('insert_Type');
    $('#form_type_action').val('Thêm mới');
    $('#add_type_form')[0].reset();
    $('#form_type_action').attr('disabled', false);
    $("#add_type_modal").modal('show');
  });
  $('#add_type_form').on('submit', function(event){
    event.preventDefault();
    var error_type = '';
    var error_unit = '';
    if($('#NAME_TYPE').val() == '')
    {
      error_type = 'Nhập Tên loại sản phẩm';
      $('#error_type').text(error_type);
      $('#NAME_TYPE').css('border-color', '#cc0000');
    }
    else
    {
      error_type = '';
      $('#error_type').text(error_type);
      $('#NAME_TYPE').css('border-color', '');
    }

    if($('#UNIT').val()=='')
    {
      error_unit = 'Nhập đơn vị';
      $('#error_unit').text(error_unit);
      $('#UNIT').css('border-color', '#cc0000'); 
    }
    else
    {
      error_unit = '';
      $('#error_unit').text(error_unit);
      $('#UNIT').css('border-color', '');
    }

    if(error_type != '' || error_unit != '')
    {
      return false;
    }
    else
    {
       // console.log( $('#NAME_SUPP').val());
        //console.log($('#COUNTRY').val());
        $('#add_type_form').attr('disabled', 'disabled');
        var form_data = $(this).serialize();
        $.ajax({
          url:"?mod=admin&act=insert_type",
          method:"POST",
          data:form_data,
          success:function(data)
          {
            if(data ==  '1')
            {
              $('#add_type_modal').modal('hide');
              load_data();
              $('#form_type_action').attr('disabled', false);
              toastr.success('Thêm thành công nha', 'SUCCESS ALERT', {timeOut: 1500});
            }
            else{
              toastr.error('Có gì đó sai sai', 'ERROR ALERT', {timeOut: 1500});
            }
          }
        });
      }

    });

  $('#add_supp').click(function(){
    $('#action_supp').val('insert_supp');
    $('#form_supp_action').val('Thêm mới');
    $('#add_supp_form')[0].reset();
    $('#form_supp_action').attr('disabled', false);
    $("#add_supp_modal").modal('show');
  });

  $('#add_supp_form').on('submit', function(event){
    event.preventDefault();
    var error_name = '';
    var error_country = '';
    if($('#NAME_SUPP').val() == '')
    {
      error_name = 'Nhập Tên nhà sản xuất';
      $('#error_supp').text(error_name);
      $('#NAME_SUPP').css('border-color', '#cc0000');
    }
    else
    {
      error_name = '';
      $('#error_supp').text(error_name);
      $('#NAME_SUPP').css('border-color', '');
    }

    if($('#COUNTRY').val()=='default')
    {
      error_country = 'Vui lòng chọn quốc gia';
      $('#error_country').text(error_country);
      $('#COUNTRY').css('border-color', '#cc0000'); 
    }
    else
    {
      error_country = '';
      $('#error_country').text(error_country);
      $('#COUNTRY').css('border-color', '');
    }

    if(error_name != '' || error_country != '')
    {
      return false;
    }
    else
    {
       // console.log( $('#NAME_SUPP').val());
        //console.log($('#COUNTRY').val());
        $('#add_supp_form').attr('disabled', 'disabled');
        var form_data = $(this).serialize();
        $.ajax({
          url:"?mod=admin&act=insert_supp",
          method:"POST",
          data:form_data,
          success:function(data)
          {
            if (data == '1') {
              $('#add_supp_modal').modal('hide');
              load_data();
              $('#form_supp_action').attr('disabled', false);
              toastr.success('Thêm thành công nha', 'SUCCESS ALERT', {timeOut: 1500});
            }
            else{
              toastr.error('Có gì đó sai sai', 'ERROR ALERT', {timeOut: 1500});
            }
          }

        });
      }

    });
});
