<?php require_once("common/model/Connection.php"); ?>
<?php
$connection = new Connection();
$conn= $connection->getConnection();
$query = "SELECT
p.`CODE_PRODUCT`,
p.`NAME_PROD`,
s.`NAME_SUPP`,
t.`NAME_TYPE`,
t.UNIT,
i.`NAME_IMG`,
stt.QUANTITY,
stt.PRICE,
stt.PROFIT
FROM
products p,
suppliers s,
types_of_product t,
images i,
`status` stt
WHERE
(
	p.CODE_SUPPLIER = s.CODE_SUPPLIER
	)
	AND (p.CODE_TYPE = t.CODE_TYPE)
	AND (p.CODE_PRODUCT = i.`CODE`)
	AND (
	p.CODE_PRODUCT = stt.CODE_PRODUCT
	)";

	$statement = $conn->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$i = 1;
	foreach ($result as $key => $value) {
		$result[$key]['index'] = $i++;
		$result[$key]['PRICE'] = number_format($result[$key]['PRICE']);
		$result[$key]['PROFIT'] = number_format($result[$key]['PROFIT']);
		$result[$key]['QUANTITY'] = number_format($result[$key]['QUANTITY']);
	}
	echo json_encode($result);
	?>