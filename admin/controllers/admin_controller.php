<?php 
/**
* 
*/
require_once('common/model/supplier.php');
require_once('common/model/type_of_product.php');
require_once('common/model/image.php');
require_once('common/model/product.php');
require_once('common/model/status.php');

// require_once('../../common/model/supplier.php');
// require_once('../../common/model/type_of_product.php');
// require_once('../../common/model/image.php');
// require_once('../../common/model/product.php');
// require_once('../../common/model/status.php');
class admin_controller
{
	function login()
	{
		require_once('admin/views/login.php');
	}
	function logout()
	{
		unset($_SESSION['admin']);
		header("Location:?mod=admin&act=login");
	}
	function login_process()
	{
		if($_POST['username'] == 'admin' && $_POST['password'] == 'admin')
		{
			$_SESSION['admin'] = array('admin', 'admin');
			echo true;
		}
		else echo false;
	}
	function profile()
	{
		require_once('admin/views/profile.php');
	}
	function all_prod()
	{
		require_once('admin/views/products_data.php');
	}
	function all_type()
	{
		require_once('admin/views/types_of_product_data.php');
	}
	function all_supp()
	{
		require_once('admin/views/supplier_data.php');
	}
	function get_all_product()
	{
		require_once('admin/models/fetch_product.php');
	}
	function get_all_type()
	{
		$type = new type_of_product();
		echo json_encode($type->All());
	}
	function get_all_supp()
	{
		$supp = new supplier();
		echo json_encode($supp->All());
	}
	function display_types_of_prod()
	{
		$type = new type_of_product();
		$list_type = $type->All();
		$output = '<select name="TYPE" id="TYPE">
		<option value="default" onchange="changeType()">Chọn loại sản phẩm</option>
		';
		foreach($list_type as $type)
		{
			$output .= '
			<option value="'.$type["NAME_TYPE"].'">'.$type["NAME_TYPE"].'</option>
			';
		}

		$output .= '</select>';
		echo $output;
	}
	function display_supp()
	{
		$supp = new supplier();
		$list_supp = $supp->All();
		$output = '<select name="SUPP" id="SUPP">
		<option value="default">Chọn nhà cung cấp</option>

		';
		foreach($list_supp as $supp)
		{
			$output .= '
			<option value="'.$supp["NAME_SUPP"].'">'.$supp["NAME_SUPP"].'</option>
			';
		}
		
		$output .= '</select>';
		echo $output;
	}
	function insert_prod()
	{
		$prod = new product();
		$type = new type_of_product();
		$supp = new supplier();
		$img = new image();
		$stt = new status();
		$key = 'NAME_PROD';
		$key1 = 'NAME_TYPE';
		$key2 = 'NAME_SUPP';
		$check = $prod->is_available($_POST['NAME_PROD'],$key);
		$check1 = $type->get_record($_POST['TYPE'],$key1);
		$check2 = $supp->get_record($_POST['SUPP'],$key2);
		if($check!=0)
		{
			echo false;
		}
		else{
			$data = array(
				'CODE_PRODUCT' => $prod->getCode(),
				'NAME_PROD' => $_POST['NAME_PROD'],
				'CODE_SUPPLIER' => $check2[0]['CODE_SUPPLIER'],
				'CODE_TYPE' => $check1[0]['CODE_TYPE'],
				'DISCRIPTION' => $_POST['DISCRIPTION']
				);
			$image = array(
				'CODE_IMG' => $img->getCode(), 
				'CODE' => $prod->getCode(), 
				'NAME_IMG' => $_SESSION['prod']['PHOTO'] 
				);
			$status = array(
				'CODE_PRODUCT' => $prod->getCode(), 
				'QUANTITY' => $_POST['QUANTITY'], 
				'PRICE' => str_replace(",","",$_POST['PRICE']), 
				'PROFIT' => $_POST['PROFIT'], 
				'PENDING' => '0', 
				'SOLD' => '0'
				);
			echo  $prod->insert($data) && $img->insert($image) && $stt->insert($status);
			unset($_SESSION['prod']['PHOTO']);
		}
	}
	function insert_supp()
	{
		$supp = new supplier();
		$key = 'NAME_SUPP';
		$check = $supp->is_available($_POST['NAME_SUPP'],$key);
		if($check!=0)
		{
			echo false;
		}
		else{
			$data = array(
				'CODE_SUPPLIER' => $supp->getCode(),
				'NAME_SUPP' => $_POST['NAME_SUPP'],
				'COUNTRY' => $_POST['COUNTRY']
				);
			echo  $supp->insert($data);
		}
	}
	function insert_type()
	{
		$type = new type_of_product();
		$key = 'NAME_TYPE';
		$check = $type->is_available($_POST['NAME_TYPE'],$key);
		if($check!=0)
		{
			echo '0';
		}
		else{
			$data = array(
				'CODE_TYPE' => $type->getCode(),
				'NAME_TYPE' => $_POST['NAME_TYPE'],
				'UNIT' => $_POST['UNIT']
				);
			echo  $type->insert($data);
		}
	}
	function upload_photo_prod()
	{
		if (isset($_POST) && !empty($_FILES['file'])) {
    $duoi = explode('.', $_FILES['file']['name']); // tách chuỗi khi gặp dấu .
    $duoi = $duoi[(count($duoi) - 1)]; //lấy ra đuôi file
    // Kiểm tra xem có phải file ảnh không
    if ($duoi === 'jpg' || $duoi === 'png' || $duoi === 'gif') {
        // tiến hành upload
    	if (move_uploaded_file($_FILES['file']['tmp_name'], 'common/img/product/' . $_FILES['file']['name'])) {
            // Nếu thành công
    		$_SESSION ['prod'] ['PHOTO'] =  $_FILES['file']['name'];
    		echo $_FILES['file']['name'];
        } else { // nếu không thành công
        	echo false;
        }
    } else { // nếu không phải file ảnh
        return 'Chỉ được upload ảnh'; // in ra thông báo
    }
} else {
    die('Lock'); // nếu không phải post method
}
}
function detail_prod()
{
	$p = new product();
	$t = new type_of_product();
	$s = new supplier();
	$i = new image();
	$st = new status();

	$product = $prod->find($_GET['id']);
	$type = $t->find($product['CODE_TYPE']);
	$supp = $s->find($product['CODE_SUPPLIER']);
	$img = $s->find($product['CODE_PRODUCT']);
	$status = $st->find($product['CODE_PRODUCT']);

	$data = array(
		'AVATAR' => $img['NAME_IMG'],
		'CODE_PRODUCT' => $product['CODE_PRODUCT'],
		'NAME_PROD' => $product['NAME_PROD'],
		'NAME_TYPE' => $type['NAME_TYPE'],
		'UNIT' => $type['UNIT'],
		'NAME_SUPP' => $supp['NAME_SUPP'],
		'COUNTRY' => $supp['COUNTRY'],
		'QUANTITY' => $status['QUANTITY'],
		'PRICE' => $status['PRICE'],
		'PROFIT' =>$status['PROFIT'],
		'SALES' => $status['SALES'],
		'SOLD' => $status['SOLD'],
		'PENDING' => $status['PENDING'],
		'DISCRIPTION' =>$product['DISCRIPTION']
		);

	echo json_encode($data);
}
function delete_prod()
{
	$prod = new product();
	$prod->setCode($_GET['id']);
	if($this->delete_status($_GET['id']) && $this->delete_img($_GET['id']))
	{
		echo $prod->delete();
	}
	else echo false;
	
}
function delete_type($id)
{
	$type = new type_of_product();
	$type->setCode($id);
	return $type->delete();
}
function delete_supp($id)
{
	$supp = new supplier();
	$supp->setCode($id);
	return $supp->delete();
}
function delete_status($id)
{
	$stt = new status();
	$stt->setCode($id);
	return $stt->delete();
}
function delete_img($id)
{
	$img = new image();
	$img->setCode($id);
	return $img->delete();
}

}
?>