<?php include_once('admin/views/layouts/header.php'); ?>
<?php include_once('common/model/countries.php') ?>
 <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        <!--main content start-->
        <section id="main-content">
          <section class="wrapper">
            <div class="row mt">
              <div class="col-md-12">
                <div class="content-panel">
                  <table class="table table-striped table-advance table-hover" id="suppliertTable">
                    <h4><i class="fa fa-angle-right"></i> Danh sách nhà sản xuất</h4>
                    <hr>
                    <button type="button" name="add_supp" id="add_supp" class="btn btn-success btn-xs" >
                      <i class="fa fa-plus"></i></button>
                      <thead  class="thead-dark">
                        <tr>
                         <th>ID</th>
                         <th>Tên nhà sản xuất</th>
                         <th>Quốc gia</th>
                       </tr>
                     </thead>
                     <tfoot>
                      <tr>
                        <tr>
                          <th>ID</th>
                          <th>Tên nhà sản xuất</th>
                          <th>Quốc gia</th>
                        </tr>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
              <!-- page end-->
            </div>
            <!-- /container -->
          </section>
          <!-- /wrapper -->
        </section>
        <!-- /MAIN CONTENT -->
        <!--main content end-->
        <?php include_once('admin/views/layouts/footer.php') ?>
