<?php include_once('admin/views/layouts/header.php'); ?>
<?php include_once('common/model/countries.php') ?>
 <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        <!--main content start-->
        <section id="main-content">
          <section class="wrapper">
            <div class="row mt">
              <div class="col-md-12">
                <div class="content-panel">
                  <table class="table table-striped table-advance table-hover" id="typeOfProductTable">
                    <h4><i class="fa fa-angle-right"></i> Danh sách Loại sản phẩm</h4>
                    <hr>
                    <button type="button" name="add_type" id="add_type" class="btn btn-success btn-xs" >
                      <i class="fa fa-plus"></i></button>
                      <thead  class="thead-dark">
                        <tr>
                         <th>ID</th>
                         <th>Tên loại sản phẩm</th>
                         <th>Đơn vị</th>
                       </tr>
                     </thead>
                     <tfoot>
                      <tr>
                        <tr>
                          <th>ID</th>
                          <th>Tên loại sản phẩm</th>
                          <th>Đơn vị</th>
                        </tr>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
              <!-- page end-->
            </div>
            <!-- /container -->
          </section>
          <!-- /wrapper -->
        </section>
        <!-- /MAIN CONTENT -->
        <!--main content end-->
        <?php include_once('admin/views/layouts/footer.php') ?>
