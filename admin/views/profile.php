 <?php include_once('layouts/header.php'); ?>
 <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        <!--main content start-->
        <section id="main-content">
          <section class="wrapper site-min-height">
            <div class="row mt">
              <div class="col-lg-12">
                <div class="row content-panel">
                  <div class="col-md-4 profile-text mt mb centered">
                    <div class="right-divider hidden-sm hidden-xs">
                      <h4>1922</h4>
                      <h6>FOLLOWERS</h6>
                      <h4>290</h4>
                      <h6>FOLLOWING</h6>
                      <h4>$ 13,980</h4>
                      <h6>MONTHLY EARNINGS</h6>
                    </div>
                  </div>
                  <!-- /col-md-4 -->
                  <div class="col-md-4 profile-text">
                    <h3>Nguyễn Văn Hiệp</h3>
                    <h6>Main Administrator</h6>
                    <div><span class="fa fa-briefcase"> Studying at </span><a href="https://www.hust.edu.vn/"> Trường Đại học Bách Khoa Hà Nội</a></div>
                    <div><span class="fa fa-home"> Lives in </span><a href="https://vi.wikipedia.org/wiki/Hà_Nội"> Hanoi, Vietnam</a></div>
                    <div><span class="fa fa-map-marker"> From </span><a href="https://vi.wikipedia.org/wiki/Nam_Định"> Nam Định, Vietnam</a></div>
                    <div><span class="fa fa-heart-o"> Single </span></div>
                    <div><span class="fa fa-mobile-phone"> Phone: +84 389289563 </span></div>
                    <div><span class="fa fa-envelope"> Email: nguyenhiepvan.bka@gmail.com</span></div>
                    <br>
                  </div>
                  <!-- /col-md-4 -->
                  <div class="col-md-4 centered profile-text">
                    <div class="profile-pic">
                      <p><img src= "common/img/admin/ui-sam.jpg" class="img-circle"></p>
                      <h4><span>I think, therefore I am</span></h4>
                    </div>
                  </div>
                  <!-- /col-md-4 -->
                </div>
                <!-- /row -->
              </div>
              <!-- /col-lg-12 -->
            </div>
            <!-- /container -->
          </section>
          <!-- /wrapper -->
        </section>
        <!-- /MAIN CONTENT -->
        <!--main content end-->
        <?php include_once('layouts/footer.php') ?>