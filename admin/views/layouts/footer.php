<!--footer start-->
<footer class="site-footer">
  <div class="container text-center">
    <p>
      &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
    </p>
    <div class="copyright text-center">

      Created with Dashio template by <a href="https://facebook.com/nguyenhiepvan.public">Hiệp nguyễn</a>
    </div>
    <a href="#" class="go-top">
      <i class="fa fa-angle-up"></i>
    </a>
  </div>
</footer>
<!--footer end-->
</div>
<!--.content-wrapper -->
</section>
<!--modal add Sản phẩm-->
<div class="modal fade" id="add_product" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle" style="text-align: center;">Thêm sản phẩm mới</h3>
      </div>
      <div class="modal-body">
        <form method="POST" id="add_prod_form">
          <div class="form-group">
            <div style="overflow: auto;">
              <img id="AVATAR" src="common/img/product/<?=$_SESSION ['prod'] ['PHOTO']?>" width = "100px" height = "100px" style= "float:left" >
              <br>
              <form id="upload_photo_form" method="POST" role="form" style="float: left;">

                <div class="form-group">
                  <label for="file">Chọn file</label>
                  <input id="file" type="file" name="sortpic"/>
                </div>
                <div class="form-group">
                  <button id="upload_photo" form="upload_photo_form" class="btn btn-primary">Tải lên</button>
                </div>
              </form>
            </div>
            <br>
            <div class="form-group">
              <label>Tên sản phẩm:</label>
              <input type="text" name="NAME_PROD" id="NAME_PROD" class="form-control">
              <span id="error_name" class="text-danger"></span>
            </div>
            <div class="form-group">
              <label>Loại sản phẩm: </label>
              <button type="button" name="add_type" id="add_type" class="btn btn-success btn-xs">
                <i class="fa fa-plus"></i></button>
                <div id="data_type"></div>
                <span id="error_type" class="text-danger"></span>
              </div>
              <div class="form-group">
                <label>Nhà sản xuất:</label>
                <button type="button" id="add_supp" name="add_supp" class="btn btn-success btn-xs">
                  <i class="fa fa-plus"></i></button>
                  <div id="data_supp"></div>
                  <span id="error_supp" class="text-danger"></span>
                </div>
                <div class="form-group">
                  <label>Số lượng:</label>
                  <input type="text" value="0" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="QUANTITY" name="QUANTITY"/>
                </div>
                <div class="form-group">
                  <label>Giá nhập:</label>
                  <input  class="money" type="text" value="0" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="PRICE" name="PRICE"/> VND
                </div>
                <div class="form-group">
                  <label>Lợi nhuận:</label>
                  <input type="text" value="0" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="PROFIT" name="PROFIT"/> %
                </div>
                <div class="form-group">
                  <label>Thông tin sản phẩm:</label>
                  <br>
                  <textarea rows="4" cols="50" name="DISCRIPTION" form="add_prod_form">
                  </textarea>
                </div>
                <div class="form-group">
                  <button id="insert_Prod" form="add_prod_form" class="btn btn-primary">Thêm mới</button>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>
      <!--modal add loại sản phẩm-->
      <div class="modal fade" id="add_type_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="exampleModalLongTitle" style="text-align: center;">Thêm Loại phẩm mới</h3>
            </div>
            <div class="modal-body">
              <form method="POST" id="add_type_form">
                <div class="form-group">
                  <label>Tên Loại sản phẩm:</label>
                  <input type="text" name="NAME_TYPE" id="NAME_TYPE" class="form-control">
                  <span id="error_type" class="text-danger"></span>
                </div>
                <div class="form-group">
                  <label>Đơn vị: </label>
                  <input type="text" name="UNIT" id="UNIT" class="form-control">
                  <span id="error_unit" class="text-danger"></span>
                </div>
                <div class="form-group">
                  <input type="hidden" name="action" id="action_type" value="insert_type" />
                  <input type="hidden" name="hidden_id" id="hidden_id" />
                  <input type="submit" name="form_type_action" id="form_type_action" class="btn btn-info" value="insert_type" />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!--modal add Nhà cung cấp-->
      <div class="modal fade" id="add_supp_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="exampleModalLongTitle" style="text-align: center;">Thêm Nhà sản xuất mới</h3>
            </div>
            <div class="modal-body">
              <form method="POST" id="add_supp_form">
                <div class="form-group">
                  <label>Tên nhà sản xuất:</label>
                  <input type="text" name="NAME_SUPP" id="NAME_SUPP" class="form-control">
                  <span id="error_supp" class="text-danger"></span>
                </div>
                <div class="form-group">
                  <label>Quốc gia: </label>
                  <select name="COUNTRY" id="COUNTRY">
                    <option value="default">Chọn quốc gia</option>
                    <?php foreach ($countries as $key => $value) {
                      ?>
                      <option value="<?=$value?>"><?=$value?></option>
                      <?php }  ?>
                    </select>
                  </div>
                  <span id="error_country" class="text-danger"></span>
                  <div class="form-group">
                    <input type="hidden" name="action" id="action_supp" value="insert_supp" />
                    <input type="hidden" name="hidden_id" id="hidden_id" />
                    <input type="submit" name="form_supp_action" id="form_supp_action" class="btn btn-info" value="insert_supp" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <!--modal thông tin Sản phẩm-->
        <div class="modal fade" id="product_detail_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLongTitle" style="text-align: center;">Thông tin sản phẩm</h3>
              </div>
              <div class="modal-body">
                <form method="POST" >
                 <div class="form-group">
                  <img id="AVATAR_detail" width = "100px" height = "100px" >
                </div>
                <div class="form-group">
                  <label>Mã sản phẩm:</label>
                  <span name="CODE_PRODUCT_detail" id="CODE_PRODUCT_detail" class="form-control"></span>
                </div>
                <div class="form-group">
                  <label>Tên sản phẩm:</label>
                  <span name="NAME_PROD_detail" id="NAME_PROD_detail" class="form-control"></span>
                </div>
                <div class="form-group">
                  <label>Loại sản phẩm: </label>
                  <span name="NAME_TYPE_detail" id="NAME_TYPE_detail" class="form-control"></span>
                </div>
                <div class="form-group">
                  <label>Đơn vị: </label>
                  <span name="UNIT_detail" id="UNIT_detail" class="form-control"></span>
                </div>
                <div class="form-group">
                  <label>Nhà sản xuất:</label>
                  <span name="NAME_SUPP_detail" id="NAME_SUPP_detail" class="form-control"></span>
                </div>
                <div class="form-group">
                  <label>Quốc gia:</label>
                  <span name="COUNTRY_detail" id="COUNTRY_detail" class="form-control"></span>
                </div>
                <div class="form-group">
                  <label>Số lượng:</label>
                  <span name="QUANTITY_detail" id="QUANTITY_detail" class="form-control"></span>
                </div>
                <div class="form-group">
                  <label>Giá nhập:</label>
                  <span name="PRICE_detail" id="PRICE_detail" class="form-control">VND</span>
                </div>
                <div class="form-group">
                  <label>Lợi nhuận:</label>
                  <span name="PROFIT_detail" id="PROFIT_detail" class="form-control">%</span>
                </div>
                <div class="form-group">
                  <label>Giá bán:</label>
                  <span name="SALES_detail" id="SALES_detail" class="form-control">VND</span>
                </div>
                <div class="form-group">
                  <label>Đã bán</label>
                  <span name="SOLD_detail" id="SOLD_detail" class="form-control"></span>
                </div>
                <div class="form-group">
                  <label>Đang đặt</label>
                  <span name="PENDING_detail" id="PENDING_detail" class="form-control"></span>
                </div>
                <div class="form-group">
                  <label>Thông tin sản phẩm:</label>
                  <br>
                  <p name="DISCRIPTION_detail" id="DISCRIPTION_detail" class="form-control"></p>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>


      <!-- js placed at the end of the document so the pages load faster -->
      <script class="include" type="text/javascript" src="admin/public/lib/jquery.dcjqaccordion.2.7.js"></script>
      <script src="admin/public/lib/jquery.scrollTo.min.js"></script>
      <script src="admin/public/lib/jquery.nicescroll.js" type="text/javascript"></script>
      <script src="admin/public/lib/jquery.sparkline.js"></script>
      <!--common script for all pages-->
      <script src="admin/public/lib/common-scripts.js"></script>
      <!--script for this page-->
      <script src="admin/public/lib/sparkline-chart.js"></script>
      <script src="admin/public/lib/zabuto_calendar.js"></script>
      <script class="include" type="text/javascript" src="admin/public/js/crud_process.js"></script>
      
      <script type="text/javascript">
        //   function detail_prod()
       //    {
       //      $("#detail_product").modal('show');
 //   $.ajax({
 //    url: '?mod=admin&act=detail_prod&id='+String(value),
 //    method: 'POST',
 //    cache: false,
 //    dataType: 'JSON',
 //    success: function(data){
 //     $('#AVATAR').attr('src','common/img/product/'+data['NAME_IMG']);
 //     $('#CODE_PRODUCT').text(data['CODE_PRODUCT']);
 //     $('#NAME_PROD').text(data['NAME_PROD']);
 //     $('#NAME_TYPE').text(data['NAME_TYPE']);
 //     $('#UNIT').text(data['UNIT']);
 //     $('#NAME_SUPP').text(data['NAME_SUPP']);
 //     $('#COUNTRY').text(data['COUNTRY']);
 //     $('#QUANTITY').text(data['QUANTITY']);
 //     $('#PRICE').text(data['PRICE']);
 //     $('#PROFIT').text(data['PROFIT']);
 //     $('#SALES').text(data['SALSE']);
 //     $('#SOLD').text(data['SOLD']);
 //     $('#PENDING').text(data['PENDING']);
 //     $('#DISCRIPTION').text(data['DISCRIPTION']);

 //   }
 // });
//}
// function detail_prod(value) {
//   $("#product_detail_modal").modal('show');
// }

</script>
</script>
<script type="text/javascript">
  $(document).ready(function(){ 

    $('.money').simpleMoneyFormat();
           //xử lý khi có sự kiện click
           $('#upload_photo').on('click', function () {
        //Lấy ra files
        var file_data = $('#file').prop('files')[0];
        //lấy ra kiểu file
        var type = file_data.type;
        //Xét kiểu file được upload
        var match = ["image/gif", "image/png", "image/jpeg", "image/jpg",];
        //kiểm tra kiểu file
        if (type == match[0] || type == match[1] || type == match[2]|| type == match[3]) {

            //khởi tạo đối tượng form data
            var form_data = new FormData();
            //thêm files vào trong form data
            form_data.append('file', file_data);
            //sử dụng ajax post
            $.ajax({
                url: '?mod=admin&act=upload_photo_prod', // gửi đến file upload.php 
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (res) {
                  $('#file').val('');
                  if(res != '0')
                  {
                   $('#AVATAR').attr('src','common/img/product/' + res);
                 }
                 else{
                  toastr.error('Có gì đó sai sai 352', 'ERROR ALERT', {timeOut: 1500});
                }
              }
            });
          } else {
            toastr.error('Có gì đó sai sai 357', 'ERROR ALERT', {timeOut: 1500});
            $('#file').val('');
          }
          return false;
        });
         })
       </script>
       
     </body>

     </html>
