<!DOCTYPE html>
<html lang="vi">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Nguyễn Hiệp">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>ADMIN GUI</title>
  <link rel="shortcut icon" href="common/img/icons/logo.ico"/>

  <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>

  <!-- Bootstrap core CSS -->

  <!--external css-->
  <link href="admin/public/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="admin/public/css/zabuto_calendar.css">

  <link rel="stylesheet" type="text/css" href="admin/public/lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href="admin/public/css/style.css" rel="stylesheet">
  <link href="admin/public/css/style-responsive.css" rel="stylesheet">
  <script src="admin/public/lib/chart-master/Chart.js"></script>
  
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
  <script src="admin/public/js/simple.money.format.js"></script>
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
        <!--header start-->
        <header class="header black-bg">
          <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
          </div>
          <!--logo start-->
          <a href="main.php" class="logo"><b>ZENT<span>GROUP</span></b></a>
          <!--logo end-->
          <div class="top-menu">
            <ul class="nav pull-right top-menu">
              <li>
                <a class="logout" href="?mod=admin&act=logout">Logout</a>
              </li>
            </ul>
          </div>
        </header>
        <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
        <!--sidebar start-->
        <aside>
          <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
              <p class="centered"><a href="?mod=admin&act=profile"><img src="common/img/admin/ui-sam.jpg" class="img-circle" width="80"></a></p>
              <h5 class="centered">Nguyễn Hiệp</h5>
              <li class="mt">
                <a class="active" href="main.php">
                  <i class="fa fa-dashboard"></i>
                  <span>Dashboard</span>
                </a>
              </li>
              <li class="sub-menu">
                <a href="javascript:;">
                  <i class="fa fa-th"></i>
                  <span>Data Tables</span>
                </a>
                <ul class="sub">                 
                  <li><a href="admin/views/custommers_data.php">Danh sách khách hàng</a></li>
                  <li><a href="admin/views/staffs_data.php">Danh sách nhân viên</a></li>
                  <li><a href="?mod=admin&act=all_prod">Danh sách sản phẩm</a></li>
                  <li><a href="?mod=admin&act=all_supp">Danh sách nhà cung cấp</a></li>
                  <li><a href="?mod=admin&act=all_type">Danh sách loại sản phẩm</a></li>
                </ul>
              </li>
              <li>
                <a href="inbox.php">
                  <i class="fa fa-envelope"></i>
                  <span>Mail </span>
                  <span class="label label-theme pull-right mail-info">2</span>
                </a>
              </li>
              <a href="google_maps.php">
                <i class="fa fa-map-marker"></i>
                <span>Google Maps </span>
              </a>
            </li>
          </ul>
          <!-- sidebar menu end-->
        </div>
      </aside>
      <!--sidebar end-->
      <div id="content-wrapper">