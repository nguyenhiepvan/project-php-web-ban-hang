<?php include_once('admin/views/layouts/header.php'); ?>
<?php include_once('common/model/countries.php') ?>
 <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
        <!--main content start-->
        <section id="main-content">
          <section class="wrapper">
            <div class="row mt">
              <div class="col-md-12">
                <div class="content-panel">
                  <table class="table table-striped table-advance table-hover" id="productTable">
                    <h4><i class="fa fa-angle-right"></i> Danh sách sản phẩm</h4>
                    <hr>
                    <button type="button" name="add_product_button" id="add_product_button" class="btn btn-success btn-xs" >
                      <i class="fa fa-plus"></i></button>
                      <thead  class="thead-dark">
                        <tr>
                          <th>#</th>
                          <th></th>
                          <th>Tên sản phẩm</th>
                          <th>Hãng sản xuất</th>
                          <th>Đơn vị</th>
                          <th>Số lượng</th>
                          <th>Giá nhập</th>
                          <th>Lợi nhuận</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>#</th>
                          <th></th>
                          <th>Tên sản phẩm</th>
                          <th>Hãng sản xuất</th>
                          <th>Đơn vị</th>
                          <th>Số lượng</th>
                          <th>Giá nhập</th>
                          <th>Lợi nhuận</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                <!-- page end-->
              </div>
              <!-- /container -->
            </section>
            <!-- /wrapper -->
          </section>

          <!-- /MAIN CONTENT -->
          <!--main content end-->
          <?php include_once('admin/views/layouts/footer.php') ?>
