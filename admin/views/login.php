<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Admin login</title>
  <!-- Bootstrap core CSS -->
  <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
  <!--external css-->
  <link href="admin/public/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="admin/public/css/style.css" rel="stylesheet">
  <link href="admin/public/css/style-responsive.css" rel="stylesheet">
</head>

<body>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <div id="login-page">
        <div class="container">
          <form class="form-login" id="login_form" method="POST">
            <h2 class="form-login-heading">sign in now</h2>
            <div class="login-wrap">
              <input type="text" id="username" name="username" class="form-control" placeholder="User ID" autofocus>
              <span id="error_username" class="text-danger"></span>
              <br>
              <input type="password" id="password" name="password" class="form-control" placeholder="Password">
              <span id="error_password" class="text-danger"></span>
<!--               <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                  <a data-toggle="modal" href="login.php#myModal"> Forgot Password?</a>
                </span>
              </label> -->
              <br>
              <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
              <hr>
<!--               <div class="login-social-link centered">
                <p>or you can sign in via your social network</p>
                <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
                <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>
              </div> -->
              <div class="registration">
                Don't have an account yet?<br/>
                <a class="" href="#">
                  Contact with admin to create account
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!--BACKSTRETCH-->
      <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
      <script type="text/javascript" src="admin/public/lib/jquery.backstretch.min.js"></script>
      <script type="text/javascript" src="admin/public/js/login.js"></script>
      <script>
        $.backstretch("common/img/admin/login-bg.jpg", {
          speed: 500
        });
      </script>
    </body>

    </html>
